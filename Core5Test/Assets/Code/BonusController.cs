﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusController : MonoBehaviour
{
    public Bonus bonus = Bonus.speed;

    [HideInInspector] public BonusState curState = BonusState.inactive;
    [HideInInspector] public float sinceBonusActivation = 0.0f;
    [HideInInspector] public int playerLayer = 9;

    private GameObject player;

    void Start()
    {
        player = GameObject.Find("Player");
    }

    void Update()
    {
        if(curState == BonusState.active)
        {
            sinceBonusActivation += Time.deltaTime;
            if (LevelManager.Instance.dataSO.bonusActivationTime - sinceBonusActivation < Mathf.Epsilon)
            {
                sinceBonusActivation = 0.0f;
                curState = BonusState.inactive;
                switch (bonus)
                {
                    case Bonus.speed:
                        player.GetComponent<PlayerController>().speedMultiplier = 1;
                        break;
                    case Bonus.score:
                        LevelManager.Instance.scoreMultiplier = 1;
                        break;
                    case Bonus.immortality:
                        player.GetComponent<PlayerController>().isImmortal = false;
                        break;
                }
                gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.layer == playerLayer)
        {
            switch (bonus)
            {
                case Bonus.speed:
                    player.GetComponent<PlayerController>().speedMultiplier = LevelManager.Instance.dataSO.speedBonusMultiplier;
                    break;
                case Bonus.score:
                    LevelManager.Instance.scoreMultiplier = LevelManager.Instance.dataSO.scoreBonusMultiplier;
                    break;
                case Bonus.immortality:
                    player.GetComponent<PlayerController>().isImmortal = true;
                    break;
            }

            transform.position = new Vector3(transform.position.x, transform.position.y, -10f);
            curState = BonusState.active;
        }
    }
}

public enum BonusState
{
    active, inactive
}

public enum Bonus
{
    speed, score, immortality
}
