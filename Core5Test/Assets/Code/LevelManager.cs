﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    public GameObject enemy;

    public GameObject speedBonus;
    public GameObject multiplyScoreBonus;
    public GameObject immortalityBonus;

    public DataSO dataSO;

    [HideInInspector] public LevelState curState = LevelState.play;
    [HideInInspector] public int scoreMultiplier = 1;
    [HideInInspector] public int totalScore = 0;

    private float spawnTimeout = 0.0f;
    private float scoreTimeout = 0.0f;
    private float bonusTimeout = 0.0f;

    void Update()
    {
        if (curState != LevelState.play)
            return;

        spawnTimeout += Time.deltaTime;
        if(dataSO.spawnTime - spawnTimeout < Mathf.Epsilon)
        {
            spawnTimeout = 0.0f;
            for(int i = 0; i < dataSO.enemyCount; i++)
            {
                GameObject en = Instantiate(enemy);
                en.transform.position = Camera.main.ViewportToWorldPoint(new Vector2(Random.Range(0f, 1f), Random.Range(0f, 1f)));
                float deltaSpeedMultiplierTime = Time.timeSinceLevelLoad / dataSO.enemySpeedMultiplierTime;
                en.GetComponent<EnemyController>().speed = dataSO.enemySpeed + dataSO.enemySpeedMultiplier * (int)deltaSpeedMultiplierTime;
            }
        }

        scoreTimeout += Time.deltaTime;
        if (dataSO.scoreTime - scoreTimeout < Mathf.Epsilon)
        {
            scoreTimeout = 0.0f;
            totalScore += dataSO.score * scoreMultiplier;
            HUDManager.Instance.UpdateScore(totalScore);
        }

        bonusTimeout += Time.deltaTime;
        if (dataSO.bonusSpawnTime - bonusTimeout < Mathf.Epsilon)
        {
            bonusTimeout = 0.0f;

            int rand = Random.Range(0, 3);
            GameObject bonus = new GameObject();
            switch (rand)
            {
                case 0:
                    bonus = Instantiate(speedBonus);
                    break;
                case 1:
                    bonus = Instantiate(multiplyScoreBonus);
                    break;
                case 2:
                    bonus = Instantiate(immortalityBonus);
                    break;
            }
            bonus.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f)));
            bonus.transform.position = new Vector3(bonus.transform.position.x, bonus.transform.position.y, 0f);
        }
    }
}

public enum LevelState
{
    play, gameover
}
