﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfoController : MonoBehaviour
{
    public GameObject positionText;
    public GameObject playerText;
    public GameObject scoreText;


    public void Init(int pos, Score score)
    {
        positionText.GetComponent<Text>().text = pos.ToString();
        playerText.GetComponent<Text>().text = score.scoreName;
        scoreText.GetComponent<Text>().text = score.score.ToString();
    }
}
