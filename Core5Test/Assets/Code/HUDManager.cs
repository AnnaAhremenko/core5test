﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : Singleton<HUDManager>
{
    public GameObject gameOverPanel;

    public GameObject scoreText;
    public GameObject timeText;

    private float timer = 0.0f;
    private int minutes = 0;
    private int seconds = 0;

    void Start()
    {
        UpdateScore(0);
    }

    void Update()
    {
        timer += Time.deltaTime;

        minutes = Mathf.FloorToInt(timer / 60F);
        seconds = Mathf.FloorToInt(timer - minutes * 60);

        timeText.GetComponent<Text>().text = string.Format("{0:0}:{1:00}", minutes, seconds);
    }

    public void OpenGameOverPanel()
    {
        gameOverPanel.SetActive(true);
    }

    public void UpdateScore(int score)
    {
        scoreText.GetComponent<Text>().text = score.ToString();
    }
}
