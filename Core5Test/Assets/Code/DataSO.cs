﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/DataSO", order = 1)]
public class DataSO : ScriptableObject
{
    public int playerSpeed;

    public int enemyCount;
    public float spawnTime;
    public int enemySpeed;
    public int enemySpeedMultiplier;
    public float enemySpeedMultiplierTime;

    public float bonusActivationTime;
    public int scoreBonusMultiplier;
    public int speedBonusMultiplier;
    public int bonusSpawnTime;

    public int score;
    public int scoreTime;

}
