﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : Singleton<MenuManager>
{
    public GameObject playButton;
    public GameObject scoresButton;

    public GameObject leaderboardPanel;

    void Start()
    {
        playButton.GetComponent<Button>().interactable = true;
        playButton.GetComponent<Button>().onClick.RemoveAllListeners();
        playButton.GetComponent<Button>().onClick.AddListener(() => OpenPlayScene());

        scoresButton.GetComponent<Button>().interactable = true;
        scoresButton.GetComponent<Button>().onClick.RemoveAllListeners();
        scoresButton.GetComponent<Button>().onClick.AddListener(() => OpenScores());
    }

    public void OpenPlayScene()
    {
        SceneManager.LoadScene(1);
    }

    public void OpenScores()
    {
        leaderboardPanel.SetActive(true);
    }
}