﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public GameObject playAgainButton;
    public GameObject exitButton;
    
    void Start()
    {
        playAgainButton.GetComponent<Button>().interactable = true;
        playAgainButton.GetComponent<Button>().onClick.RemoveAllListeners();
        playAgainButton.GetComponent<Button>().onClick.AddListener(() => PlayAgain());

        exitButton.GetComponent<Button>().interactable = true;
        exitButton.GetComponent<Button>().onClick.RemoveAllListeners();
        exitButton.GetComponent<Button>().onClick.AddListener(() => OpenMenu());
    }

    void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void OpenMenu()
    {
        SceneManager.LoadScene(0);
    }
}
