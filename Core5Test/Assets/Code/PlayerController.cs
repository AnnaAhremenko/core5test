﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [HideInInspector] public bool isImmortal;
    [HideInInspector] public int speedMultiplier = 1;
    [HideInInspector] public int enemyLayer = 8;

    void Update()
    {
        if (LevelManager.Instance.curState != LevelState.play)
            return;

        if (Input.GetMouseButton(0))
        {
            Vector2 mousePos = Input.mousePosition;

            Vector2 direction = new Vector2(
                mousePos.x - transform.position.x,
                mousePos.y - transform.position.y);

            transform.up = direction;

            transform.position = Vector2.MoveTowards(transform.position, Camera.main.ScreenToWorldPoint(Input.mousePosition), LevelManager.Instance.dataSO.playerSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.layer == enemyLayer)
        {
            if (!isImmortal)
            {
                LevelManager.Instance.curState = LevelState.gameover;
                HUDManager.Instance.OpenGameOverPanel();
            }
            else
            {
                Destroy(col.gameObject);
            }
        }
    }
}
