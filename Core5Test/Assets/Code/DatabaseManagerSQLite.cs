﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;

public class DatabaseManagerSQLite : DatabaseManager
{
    private SqliteConnection dbcon;

    public override void Initialize()
    {
        if(dbcon != null && dbcon.State != ConnectionState.Closed)
            return;
        
        string connectionString = "";
#if UNITY_EDITOR
        connectionString = "URI=file:" + Application.streamingAssetsPath + "/Scores.db";
#else
			string localDB;
#if UNITY_ANDROID
            localDB = Application.streamingAssetsPath + "/Scores.db";
            WWW loadDB = new WWW (localDB);
            while (!loadDB.isDone)
            {
                Debug.Log("LOADING...");
            }
            File.WriteAllBytes(Application.persistentDataPath + "/Scores.db", loadDB.bytes);
            connectionString = "URI=file:" + Application.persistentDataPath + "/Scores.db";
#elif UNITY_IOS
            localDB = Application.streamingAssetsPath + "/Scores.db";
            File.Copy(localDB, Application.persistentDataPath + "/Scores.db", true);
            connectionString = "URI=file:" + Application.persistentDataPath + "/Scores.db";
#endif
#endif
        dbcon = new SqliteConnection(connectionString);
        dbcon.Open();
        Debug.Log("DatabaseManagerSQLite: Initialized");
    }

    public override void AddScore(Score score)
    {
        string sql = "insert into Scores (Name, Score) " +
                     "values ('" +
                     score.scoreName + "', " +
                     score.score + ");";

        SqliteCommand command = new SqliteCommand(sql, dbcon);
        command.ExecuteNonQuery();
    }

    public override List<Score> GetScores()
    {
        string sql = "select * from Scores order by Score desc;";

        List<Score> scores = new List<Score>();

        SqliteCommand command = new SqliteCommand(sql, dbcon);
        SqliteDataReader reader = command.ExecuteReader();
        while (reader.Read())
        {
            Score challenge = new Score
            {
                id = reader.GetInt64(0),
                scoreName = reader.GetString(1),
                score = reader.GetInt32(2)
            };
            scores.Add(challenge);
        }

        Debug.Log(sql);
        return scores;
    }
}
