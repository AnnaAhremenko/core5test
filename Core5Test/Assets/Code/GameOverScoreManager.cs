﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScoreManager : MonoBehaviour
{
    public GameObject inputNameField;
    public GameObject saveButton;

    public GameObject gameOverPanel;

    void Start()
    {
        saveButton.GetComponent<Button>().interactable = true;
        saveButton.GetComponent<Button>().onClick.RemoveAllListeners();
        saveButton.GetComponent<Button>().onClick.AddListener(() => SaveScore());
    }

    void Update()
    {
        
    }

    void SaveScore()
    {
        Score score = new Score
        {
            score = LevelManager.Instance.totalScore,
            scoreName = inputNameField.GetComponent<InputField>().text
        };

        DatabaseManager DB = new DatabaseManagerSQLite();
        DB.Initialize();
        DB.AddScore(score);

        gameOverPanel.SetActive(true);
        gameObject.SetActive(false);
    }
}
