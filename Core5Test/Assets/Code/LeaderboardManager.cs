﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardManager : Singleton<LeaderboardManager>
{
    public GameObject scrollViewContent;
    public GameObject playerInfoPanel;
    public GameObject backButton;

    private List<PlayerInfoController> playerInfoPanels;

    void Start()
    {
        backButton.GetComponent<Button>().interactable = true;
        backButton.GetComponent<Button>().onClick.RemoveAllListeners();
        backButton.GetComponent<Button>().onClick.AddListener(() => ClosePanel());

        playerInfoPanels = new List<PlayerInfoController>();
        ShowScores();
    }

    private void ShowScores()
    {
        DatabaseManager DB = new DatabaseManagerSQLite();
        DB.Initialize();
        List<Score> scores = DB.GetScores();

        playerInfoPanels.Clear();
        ClearScrollViewContent();
        for(int i = 0; i < scores.Count; i++)
        {
            GameObject piPanel = Instantiate(playerInfoPanel, scrollViewContent.transform, false);
            var pic = piPanel.GetComponent<PlayerInfoController>();
            pic.Init(i + 1, scores[i]);
            playerInfoPanels.Add(pic);
        }
    }

    private void ClearScrollViewContent()
    {
        foreach (Transform t in scrollViewContent.transform)
        {
            Destroy(t.gameObject);
        }
    }

    private void ClosePanel()
    {
        gameObject.SetActive(false);
    }
}
