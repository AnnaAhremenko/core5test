﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class DatabaseManager 
{
    abstract public void Initialize();

    abstract public void AddScore(Score score);
    abstract public List<Score> GetScores();
}
